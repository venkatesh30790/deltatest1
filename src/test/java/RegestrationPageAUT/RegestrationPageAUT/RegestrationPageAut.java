package RegestrationPageAUT.RegestrationPageAUT;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RegestrationPageAut  {

	public  WebDriver driver;
	
	
	
	@BeforeMethod
	
	public void beforeMethod(){
		
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Venkatesh\\Downloads\\geckodriver\\geckodriver.exe");	
		driver = new FirefoxDriver();
		
		driver.get("http://adjiva.com/qa-test/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
		//driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.);
		
	}
	
	
	@Test(priority=1)
	public void registerationwithalldata(){
		
		driver.findElement(By.xpath("//input[@name='first_name']")).sendKeys("Venkatesh");
		driver.findElement(By.xpath("//input[@name='last_name']")).sendKeys("Venkatesh");
		WebElement element = driver.findElement(By.xpath("//select[@name='department']"));
		
		Select select = new Select(element);
		select.selectByVisibleText("Engineering");
		
		driver.findElement(By.xpath("//input[@name='user_name']")).sendKeys("Venkatesh30790");
		driver.findElement(By.xpath("//input[@name='user_password']")).sendKeys("1234456789");
		driver.findElement(By.xpath("//input[@name='confirm_password']")).sendKeys("123456789");
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("venkatesh30790@gmail.com");
		
		driver.findElement(By.xpath("//input[@name='contact_no']")).sendKeys("1234567890");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.xpath("//b[text()='Thanks']")).getText();
		
		if (text.matches("Thanks")){
		}else{
			System.out.println("Registration not successful");
		}
	}
		
	@Test(priority=2)
	public void registerationwithMandatoryfield(){
		
		driver.findElement(By.xpath("//input[@name='first_name']")).sendKeys("Venkatesh");
		driver.findElement(By.xpath("//input[@name='last_name']")).sendKeys("Venkatesh");
		WebElement element = driver.findElement(By.xpath("//select[@name='department']"));
		
		Select select = new Select(element);
		select.selectByVisibleText("Engineering");
		
		driver.findElement(By.xpath("//input[@name='user_name']")).sendKeys("Venkatesh30790");
		driver.findElement(By.xpath("//input[@name='user_password']")).sendKeys("1234456789");
		driver.findElement(By.xpath("//input[@name='confirm_password']")).sendKeys("123456789");
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("venkatesh30790@gmail.com");
		
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.xpath("//b[text()='Thanks']")).getText();
		
		if (text.matches("Thanks")){
			System.out.println("Registartion Successful with only mandatory field");
		}else{
			System.out.println("Registration not successful");
		}
		
		
		
	}
	
	
	
	
	
	@Test(priority=3)
	public void registering_userwithout_EnteringAnyData(){
	
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		
		
		try{
		boolean displayed = driver.findElement(By.xpath("//b[text()='Registration Form']")).isDisplayed();
		
		if(displayed == true){
			System.out.println("Registration not happend as mandatory field are not entered");
		}else{
			System.out.println("Restration successful");
		}
		}catch(Exception e){
			boolean displayed = driver.findElement(By.xpath("//b[text()='Thanks']")).isDisplayed();
			if(displayed == true){
				System.out.println("Registration happend successfully");
			}else{
				System.out.println("Restration not successful");
			}
			
		}
	}
	
	
	@Test(priority=4)
	public void registering_user_withmissing_Mandatoryfield(){
		
		
		driver.findElement(By.xpath("//input[@name='first_name']")).sendKeys("Venkatesh");
		driver.findElement(By.xpath("//input[@name='last_name']")).sendKeys("Venkatesh");
		WebElement element = driver.findElement(By.xpath("//select[@name='department']"));
		
		Select select = new Select(element);
		select.selectByVisibleText("Engineering");
		
		driver.findElement(By.xpath("//input[@name='user_name']")).sendKeys("Venkatesh30790");
		driver.findElement(By.xpath("//input[@name='user_password']")).sendKeys("1234456789");
		driver.findElement(By.xpath("//input[@name='confirm_password']")).sendKeys("123456789");
		//driver.findElement(By.xpath("//input[@name='email']")).sendKeys("venkatesh30790@gmail.com");
		
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		boolean text1 = driver.findElement(By.xpath("//small[text()='Please enter your Email Address']")).isDisplayed();
		
		if (text1 == true){
			System.out.println("Registartion is not Successful as mandatory field email id is not entered");
		}else{
			System.out.println("Registration successful");
		}
		
	}
	
	
	
	@Test(priority=5)
	public void registering_already_registered_user(){
		driver.findElement(By.xpath("//input[@name='first_name']")).sendKeys("Venkatesh");
		driver.findElement(By.xpath("//input[@name='last_name']")).sendKeys("Venkatesh");
		WebElement element = driver.findElement(By.xpath("//select[@name='department']"));
		
		Select select = new Select(element);
		select.selectByVisibleText("Engineering");
		
		driver.findElement(By.xpath("//input[@name='user_name']")).sendKeys("Venkatesh30790");
		driver.findElement(By.xpath("//input[@name='user_password']")).sendKeys("1234456789");
		driver.findElement(By.xpath("//input[@name='confirm_password']")).sendKeys("123456789");
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("venkatesh30790@gmail.com");
		
		driver.findElement(By.xpath("//input[@name='contact_no']")).sendKeys("1234567890");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.xpath("//b[text()='Thanks']")).getText();
		
		if (text.matches("Thanks")){
			System.out.println("Registration successful");
		}else{
			System.out.println("Registration not successful");
		}
		
	}
	
	@Test(priority=6)
	public void registeringuser_byvarying_pwdandconpwd(){
		
		driver.findElement(By.xpath("//input[@name='first_name']")).sendKeys("Venkatesh");
		driver.findElement(By.xpath("//input[@name='last_name']")).sendKeys("Venkatesh");
		WebElement element = driver.findElement(By.xpath("//select[@name='department']"));
		
		Select select = new Select(element);
		select.selectByVisibleText("Engineering");
		
		driver.findElement(By.xpath("//input[@name='user_name']")).sendKeys("Venkatesh30790");
		driver.findElement(By.xpath("//input[@name='user_password']")).sendKeys("1234456789");
		driver.findElement(By.xpath("//input[@name='confirm_password']")).sendKeys("123456759");
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("venkatesh30790@gmail.com");
		
		driver.findElement(By.xpath("//input[@name='contact_no']")).sendKeys("1234567890");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.xpath("//b[text()='Thanks']")).getText();
		
		if (text.matches("Thanks")){
			System.out.println("Registration successful");
		}else{
			System.out.println("Registration not successful");
		}
		
		
	}
	
	@Test(priority=7)
	
	public void Registering_user_by_checking_field_validation(){
		
		driver.findElement(By.xpath("//input[@name='first_name']")).sendKeys("Venkatesh");
		driver.findElement(By.xpath("//input[@name='last_name']")).sendKeys("V");
		WebElement element = driver.findElement(By.xpath("//select[@name='department']"));
		
		Select select = new Select(element);
		select.selectByVisibleText("Engineering");
		
		driver.findElement(By.xpath("//input[@name='user_name']")).sendKeys("Venkatesh30790");
		driver.findElement(By.xpath("//input[@name='user_password']")).sendKeys("1234456789");
		driver.findElement(By.xpath("//input[@name='confirm_password']")).sendKeys("123456759");
		driver.findElement(By.xpath("//input[@name='email']")).sendKeys("venkatesh30790@gmail.com");
		
		driver.findElement(By.xpath("//input[@name='contact_no']")).sendKeys("1234567890");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.xpath("//small[text()='This value is not valid']")).getText();
		
		if (text.matches("This value is not valid")){
			System.out.println("Registration is not successful as lastname is receving character less than the mentioned");
		}else{
			System.out.println("Registration successful");
		}
	}
	
	
	
	
	
	
		@AfterMethod
		public void afterMethod(){
			driver.close();
		}
		
	}
	
	
	
	
	



